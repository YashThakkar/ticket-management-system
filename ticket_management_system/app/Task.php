<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'task',
        'user_id',
        'team_id',
        'team_leader_id'
    ];
}
