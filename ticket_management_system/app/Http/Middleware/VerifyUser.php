<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class VerifyUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = auth()->user()->role;
        // dd($role);
        if($role === 'admin') {
            // dd("Admin logged in...");
            return redirect(route('admin.index'));
        }
        if($role === 'member'){
            // dd("member logged in...");
            return redirect(route('member.index'));
        }
        else{
            return redirect(abort(401));
        }

        
        return $next($request);
    }
}
