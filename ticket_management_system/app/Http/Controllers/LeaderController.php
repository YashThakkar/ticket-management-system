<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Team;
use App\Task;
use App\User;
use App\User_teams;

class LeaderController extends Controller
{
    public function index()
    {
        
    //     // --------- BEST EMPLOYEE Alogrithm-----------
    //     $leader_id = auth()->user()->id;
    //     $team_id = Team::where('team_leader_id', $leader_id)->get()[0]->id;
    //     $team_members = User_teams::where('teams_id', $team_id)->get();
    //     $i=0;
    //     $hours_array = array();
    //     $task_count = array();
    //     $avg_hours = array();
    //     $user_array = array();
    //     $j = 0;
    //     foreach($team_members as $member){
    //         $each_user = Task::where([
    //             'user_id'=> $member->users_id,
    //             'task_status'=>'completed'
    //         ])->get();
    //         if($each_user != null){
                
    //             if(sizeOf($each_user) > 1){
    //                 $hours_array[$i] = $each_user->first()->created_at->diffInHours($each_user->last()->updated_at);
    //                 $task_count[$j] = sizeOf($each_user);
    //                 $user_array[$i] = $each_user;
    //                 $i++;
    //                 $j++;
    //             }
    //             elseif(sizeOf($each_user) === 1){
    //                 $hours_array[$i] = $each_user[0]->created_at->diffInHours($each_user[0]->updated_at);
    //                 $task_count[$j] = sizeOf($each_user);
    //                 $user_array[$i] = $each_user;
    //                 $i++;
    //                 $j++;
    //             }
    //             // $hours_array[$i] = $user->created_at->diffInHours($user->updated_at);
    //         }
    //     }
        
    //     // dd(sizeOf($hours_array));
    //     // dd(($hours_array));
    //     // dd(($task_count));
    //    for($k=0;$k<sizeOf($hours_array);$k++){
    //         if($task_count[$k] > 1){
    //             $avg_hours[$k] = $hours_array[$k] / $task_count[$k];
    //         }
    //         else if($task_count[$k] == 1){
    //             $avg_hours[$k] = $hours_array[$k];
    //         }
    //    }
    //    $indx = array_search(min($avg_hours), $avg_hours);
    //    //This gives users which is best employee
       
    //    $best_member = $user_array[$indx];
    //    $best_member_user = User::where('id', $best_member[0]->user_id)->get();
    // //    dd($best_member_user[0]);
    //    // --------- BEST EMPLOYEE Alogrithm-----------

        $team = Team::where('team_leader_id', auth()->user()->id)->get();
        $users_in_team = User_teams::where('teams_id', $team[0]->id)->get();
        // dd($users_in_team);

        // auth()->user()->unreadNotifications->markAsRead();
        $notifications = auth()->user()->unreadNotifications;
        // dd($notifications);

        return view('leader.index', compact([
            'team',
            'users_in_team',
            'notifications'
        ]));
    }
}
