<?php

namespace App\Http\Controllers;

use App\Task;
use App\Team;
use App\User_teams;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    public function index()
    {
        // dd(auth()->user()->id);
        
        $tasks = Task::where('user_id', auth()->user()->id)->get();
        if(sizeOf($tasks) < 0) {
            $leader_id = $tasks[0]->team_leader_id;
            $leader_name = Team::where('team_leader_id', $leader_id)->get()[0]->team_leader_name;
        } else {
            $team_id = User_teams::where('users_id', auth()->user()->id)->get()[0]->teams_id;
            $leader_id = Team::where('id', $team_id)->get()[0]->team_leader_id;
            $leader_name = Team::where('team_leader_id', $leader_id)->get()[0]->team_leader_name;
        }
        
        
        return view('member.index', compact([
            'tasks',
            'leader_name'
        ]));
    }
}
