<?php

namespace App\Http\Controllers;

use App\Http\Requests\team\CreateTeamRequest;
use App\Team;
use App\User;
use App\User_teams;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = Team::all();
        // $user_team = User_teams::all();
        // $users =  User::all();
        return view('team.index', compact([
            'teams'
            // 'user_team',
            // 'users'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::where('in_team', 0)->where('role', "member")->get();
        // dd($users = User::where('in_team', 0)->get());

        return view('team.create', compact([
            'users'
        ]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTeamRequest $request)
    {
        $team_leader_name = User::where('id', $request->team_leader)->get('name');
       $team_data =  Team::create([
            'team_name'=>$request->team_name,
            'team_leader_id'=>$request->team_leader[0],
            'team_leader_name'=>$team_leader_name[0]->name,
            'published_at'=>$request->published_at
        ]);

        //Updating role to leader in users table
        User::update_role_to_leader($request->team_leader[0]);

        //updating the in_team status in User table
        for($i = 0; $i < sizeOf($request->team_members); $i++){
            User::update_in_team_status($request->team_members[$i]);

            User_teams::create([
                'users_id' =>$request->team_members[$i],
                'teams_id'=>$team_data->id
             ]);
        }
        

        return redirect(route('team.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function show(Team $team)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function edit(Team $team)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Team $team)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function destroy(Team $team)
    {
        //
    }
}
