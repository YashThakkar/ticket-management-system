<?php

namespace App\Http\Controllers;

use App\Notifications\Notifications\TaskSubmitted;
use App\Task;
use App\Team;
use App\User;
use App\User_teams;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;

class TaskController extends Controller
{
    use Notifiable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd("inside task index");

        $team = Team::where('team_leader_id', auth()->user()->id)->get();
        $users_in_team = User_teams::where('teams_id', $team[0]->id)->get();

        auth()->user()->unreadNotifications->markAsRead();
        
        return view('task.index', compact([
            'team',
            'users_in_team'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function create()
    {

        // -------------------- BEST EMPLOYEE Alogrithm-------------------------
        $leader_id = auth()->user()->id;
        $team_id = Team::where('team_leader_id', $leader_id)->get()[0]->id;
        $team_members = User_teams::where('teams_id', $team_id)->get();
        $i=0;
        $hours_array = array();
        $task_count = array();
        $avg_hours = array();
        $user_array = array();
        $j = 0;
        foreach($team_members as $member){
            $each_user = Task::where([
                'user_id'=> $member->users_id,
                'task_status'=>'completed'
            ])->get();
            if($each_user != null){
                
                if(sizeOf($each_user) > 1){
                    $hours_array[$i] = $each_user->first()->created_at->diffInHours($each_user->last()->updated_at);
                    $task_count[$j] = sizeOf($each_user);
                    $user_array[$i] = $each_user;
                    $i++;
                    $j++;
                }
                elseif(sizeOf($each_user) === 1){
                    $hours_array[$i] = $each_user[0]->created_at->diffInHours($each_user[0]->updated_at);
                    $task_count[$j] = sizeOf($each_user);
                    $user_array[$i] = $each_user;
                    $i++;
                    $j++;
                }
                // $hours_array[$i] = $user->created_at->diffInHours($user->updated_at);
            }
        }
        
        // dd(sizeOf($hours_array));
        // dd(($hours_array));
        // dd(($task_count));
       for($k=0;$k<sizeOf($hours_array);$k++){
            if($task_count[$k] > 1){
                $avg_hours[$k] = $hours_array[$k] / $task_count[$k];
            }
            else if($task_count[$k] == 1){
                $avg_hours[$k] = $hours_array[$k];
            }
       }
       $indx = array_search(min($avg_hours), $avg_hours);

       //This gives users which is best employee
       $best_member = $user_array[$indx];
       $best_member_user = User::where('id', $best_member[0]->user_id)->get()[0];
    //    dd($best_member_user);
       // ------------------- BEST EMPLOYEE Alogrithm-------------------------


        $team_id = Team::where('team_leader_id', auth()->user()->id)->get()[0]->id;
        $users_id = User_teams::where('teams_id', $team_id)->get();
        // dd($users_id[1]->users_id);
        return view('task.create', compact([
            'users_id',
            'best_member_user'
        ]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->member[0]);
        // dd($request->task[0]);

        $team_leader_id = auth()->user()->id;
        $team_id = Team::where('team_leader_id', $team_leader_id)->get()[0]->id;
        $totaltasks = $request->total_tasks;
        
        // foreach($totaltasks as $task)
        for($i = 0; $i < $totaltasks; $i++)
        {
            $user_id = $request->member[$i];
            $task = $request->task[$i];

            Task::create([
                'task' => $task,
                'user_id' => $user_id,
                'team_id' => $team_id,
                'team_leader_id' => $team_leader_id
            ]);
        }
        return redirect(route('task.index'));
    }
    public function storeBestMemberTask(Request $request)
    {
        // dd($request->member[0]);
        // dd($request->bestTotalTasks);
        $team_leader_id = auth()->user()->id;
        $team_id = Team::where('team_leader_id', $team_leader_id)->get()[0]->id;
        $totaltasks = $request->bestTotalTasks;
        $user_id = $request->member[0]; // as it will be same for all
        
        for($i = 0; $i < $totaltasks; $i++)
        {
            $task = $request->task[$i];

            Task::create([
                'task' => $task,
                'user_id' => $user_id,
                'team_id' => $team_id,
                'team_leader_id' => $team_leader_id
            ]);
        }
        return redirect(route('task.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        //
    }

    public function update_to_approval(Request $request, $task)
    {
        // dd($task);
        Task::where("id", $task)->update(["task_status" => "approval_pending"]);
        $task_info = Task::where("id", $task)->get()[0];
        $leader = User::where('id', $task_info->team_leader_id)->get()[0];
        $user = User::where('id', auth()->user()->id)->get()[0];
        //Creating notification object
        $leader->notify(new TaskSubmitted($task_info, $user));
        return redirect()->back();
    }
    public function update_reject_or_approve(Request $request, $task)
    {
          
          if($request->status_radio === 'reject')
          {
            Task::where("id", $task)->update(["task_status" => "pending"]);
          }
          elseif($request->status_radio === 'approve')
          {
            Task::where("id", $task)->update(["task_status" => "completed"]);
          }
        
        return redirect()->back();
    }
}
