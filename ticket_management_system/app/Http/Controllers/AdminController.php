<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use App\Team;
use App\User;

class AdminController extends Controller
{
    public function index()
    {
        // return view('posts.index', compact([
        //     'posts'
        // ]));
        $teams_count = Team::all()->count();
        $teams = Team::all();
        $member_count = User::where('role', 'member')->get()->count();
        $task_completed_count = Task::where('task_status', 'completed')->get()->count();
        $task_pending_count = Task::where('task_status', 'pending')->get()->count();
        return view('admin.index', compact([
            'teams_count',
            'teams',
            'member_count',
            'task_completed_count',
            'task_pending_count'
        ]));
    }
}
