<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Team extends Model
{
    use SoftDeletes;

    protected $dates = ['published_at'];

    protected $fillable = [
        'team_name',
        'team_leader_id',
        'team_leader_name'
    ];

    // public function users() {
    //     return $this->belongsToMany(Users::class);
    // }
}
