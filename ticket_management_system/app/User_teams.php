<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_teams extends Model
{
    protected $fillable = [
        'users_id',
        'teams_id'
    ];
}
