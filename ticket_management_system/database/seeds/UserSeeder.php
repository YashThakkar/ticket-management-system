<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::where('email','yashthakkar2700@gmail.com')->get()->first();
        if(!$user){
            \App\User::create([
                'name'=>'Yash Thakkar',
                'email'=>'yashthakkar2700@gmail.com',
                'password'=>\Illuminate\Support\Facades\Hash::make('yashthakkar'),
                'role'=>'admin'
            ]);
        } else {
            $user->update(['role'=>'admin']);
        }

        \App\User::create([
            'name'=>'Jhone Doe',
            'email'=>'jhon@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('yashthakkar'),
        ]);

        \App\User::create([
            'name'=>'Jash Doshi',
            'email'=>'jash@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('yashthakkar'),
        ]);
        \App\User::create([
            'name'=>'raj panchal',
            'email'=>'raj@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('yashthakkar'),
        ]);
        \App\User::create([
            'name'=>'Ashay Gogri',
            'email'=>'ashay@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('yashthakkar'),
        ]);
        \App\User::create([
            'name'=>'Chintan Gohil',
            'email'=>'chintan@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('yashthakkar'),
        ]);
        \App\User::create([
            'name'=>'Martin Joe',
            'email'=>'martin@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('yashthakkar'),
        ]);
        \App\User::create([
            'name'=>'Chris Hemsworth',
            'email'=>'chris@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('yashthakkar'),
        ]);
        \App\User::create([
            'name'=>'Salva soe',
            'email'=>'salva@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('yashthakkar'),
        ]);
        \App\User::create([
            'name'=>'Julie doe',
            'email'=>'julie@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('yashthakkar'),
        ]);
    }
}
