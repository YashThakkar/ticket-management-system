<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

/*
    3 Interfaces:
    - ADMIN
    - LEADER
    - MEMBER
*/


Route::middleware(['auth'])->group(function(){  //, 'verifyUser'

    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/admin', 'AdminController@index')->name('admin.index');
    Route::get('/member', 'MemberController@index')->name('member.index');
    Route::put('/member/{member}', 'TaskController@update_to_approval')->name('task.update_to_approval');
    Route::resource('team', 'TeamController');
    Route::resource('task', 'TaskController');
    Route::post('/task/create', 'TaskController@storeBestMemberTask')->name('task.storeBestMemberTask');
    Route::put('/task/{task}', 'TaskController@update_reject_or_approve')->name('task.update_reject_or_approve');

    Route::get('/leader', 'LeaderController@index')->name('leader.index');
});