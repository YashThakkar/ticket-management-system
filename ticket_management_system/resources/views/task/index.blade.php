
@extends('layouts.components.layout')
@section('title','Ticket management | Ease to manage')

@section('main-content')



<div class="container mt-4">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between">
    <h1 class="h3 mb-4 text-gray-800">All Tasks <span class=""></span></h1>
    <a href="" class="d-none d-sm-inline-nlock btn btn-sm btn-primary shadow-sm">
        <i class="fas fa-list-ul fa-sm text-white"></i>
    </a>                  
    </div>

    <h4 style="font-weight: 500;">
        <span style="color: #3857F5;">{{$team[0]->team_name}}</span> Team <span>{{$team[0]->team_leader_name}} - Leader</span>
    </h4>

        @foreach($users_in_team as $user)

            <?php
                $user_info = App\User::where([
                    'id' => $user->users_id,
                    'role' => 'member'
                ])->get();
            ?>
             @if(count($user_info) > 0)

                <div class="card shadow mb-3" style="max-width: 100%; border-radius: 12px!important">
                    <div class="card-header" style="border-top-left-radius: 12px!important;border-top-right-radius: 12px!important;">
                        {{ $user_info[0]->name }} / {{ $user_info[0]->role }}
                    </div>
                    <div class="card-body">
                        <h5 class="card-title" style="color: #3857F5;">Tasks</h5>

                        <?php
                            $users = App\Task::where('user_id', $user_info[0]->id)->get();
                        ?>
                        @foreach($users as $user)
                            <div class="row mt-3">
                                <?php
                                    $color = rand(1,3);
                                ?>
                                <div class="col-md-12 pr-4">
                                    <div class="task-bar  border_color_{{ $color }}">
                                        <div class="row">
                                            <div class="col-md-10 py-3">
                                                <div class="row">
                                                    <div class="col-md-6 ">
                                                        <p class="text-muted px-3 m-0" style="font-size: 12px;">
                                                            Created at: {{ $user->created_at->diffForHumans() }}
                                                        </p>
                                                    </div>
                                                    @if($user->task_status === 'completed' || $user->task_status === 'approval_pending')
                                                    <div class="col-md-6 text-right">
                                                        <p class="text-muted px-3 m-0" style="font-size: 12px;">Submitted at: {{ $user->updated_at->diffForHumans() }}</p>
                                                    </div>
                                                    @endif
                                                </div>
                                                <span class="px-3 m-0 py-2">{{ $user->task }}</span> &nbsp;
                                                @if($user->task_status === 'pending')
                                                    <span class="fas fa-exclamation-circle color-red"></span>
                                                @endif
                                            </div>
                                            @if($user->task_status === 'pending')
                                                <div class="col-md-2 status-outer bg-danger p-0">
                                                    <p class="py-4 px-3 m-0 text-center" style="color:#fff;">Not submitted yet &nbsp;<span class="fas fa-exclamation"></span></p>
                                                </div>
                                                
                                            @elseif($user->task_status === 'approval_pending')
                                                <div class="col-md-2 status-outer bg-warning p-0">
                                                    <p class="py-4 px-3 m-0 text-center">Approval Pending &nbsp;<span class="fas fa-check-circle"></span></p>
                                                </div>
                                            @elseif($user->task_status === 'completed')
                                                <div class="col-md-2 status-outer bg-success p-0">
                                                    <p class="py-4 px-3 m-0 text-center" style="color: #fff;">Task Completed &nbsp;<span class="fas fa-check"></span></p>
                                                </div>
                                            @endif
                                        </div>
                                        
                                    </div>   
                                </div>
                                @if($user->task_status === 'approval_pending')
                                    <div class="col-md-12 pr-4 approve-box">
                                        <div class="row ml-1 approve-row">
                                            <div class="col-md-12 p-3">
                                                <p class="aprove-title m-0">Status Approval</p>
                                                <form action="{{ route('task.update_reject_or_approve', $user->id) }}" method="POST">
                                                    @csrf
                                                    @method('PUT')
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="status_radio" id="inlineRadio1" value="approve">
                                                        <label class="form-check-label" for="inlineRadio1">Approve</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="status_radio" id="inlineRadio2" value="reject">
                                                        <label class="form-check-label" for="inlineRadio2">Reject</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <button type="submit" class="btn btn-primary mb-2">Submit</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
        @endforeach
</div>
@endsection


@section('page-level-styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
@endsection


