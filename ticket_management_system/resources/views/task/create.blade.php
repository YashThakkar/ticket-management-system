
@extends('layouts.components.layout')
@section('title','Ticket management | Ease to manage')

@section('main-content')
<!-- Begin Page Content -->
<div class="container-fluid mt-4">

     <!-- Page Wrapper -->
  <div id="wrapper">

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

    <!-- Begin Page Content -->
    <div class="container-fluid">

        
      <!-- Page Heading -->
      <div class="d-sm-flex align-items-center justify-content-between">
        <h1 class="h3 mb-4 text-gray-800">Assign Task</h1>                 
      </div>
      
      <div class="container-fluid">
          <div class="row">
              <div class="col-md-12">

              <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">
                            <i class="fas fa-plus"></i>Auto Assign to Best Member
                        </h6>
                        <button type="button"
                                onclick="addBestMemberTask();"
                                class="d-sm-inline-block btn btn-sm btn-secondary shadow-sm">
                            <i class="fas fa-plus fa-sm text-white"></i>Add Task
                        </button>
                    </div>
                    <!--END OF CARD HEADER-->
                    
                    <!--CARD BODY-->
                    <form action="{{ route('task.storeBestMemberTask') }}" method="POST">
                        @csrf
                        <div class="card-body">
                           <div id="best_tasks_container">
                            <!--BEGIN: TASK CUSTOM CONTROL-->
                            <div class="row best_task_row" id="element_1">
                                <!--BEGIN: TEAM MEMBER SELECT-->
                                <div class="col-md-3">
                                    <div class="form-group">
                                       <label for="">Team Member</label>
                                       <select name="member[]" id="member_1" class="form-control member_select" readonly>
                                            <option value="{{$best_member_user->id}}" selected>{{ $best_member_user->name }}</option>
                                       </select>
                                    </div>
                                </div>
                                <!--END: TEAM MEMBER SELECT-->
                                <!--BEGIN: TASK-->
                                <div class="col-md-8">
                                    <div class="form-group">
                                       <label for="">Task</label>
                                       <input type="text" name="task[]" id="task_1"
                                               class="form-control task  @error('task') is-invalid @enderror"
                                               placeholder="Enter the task to be assigned"  value="{{ old('task') }}">
                                        @error('task')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <!--END: TASK-->
                                
                                <!--BEGIN: DELETE BUTTON-->
                                <div class="col-md-1">
                                   <button onclick="deleteBestTask(1)"
                                       type="button"
                                        class="btn btn-danger"
                                        style="margin-top: 45%">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                </div>
                                <!--END: DELETE BUTTON-->
                            </div>
                            <!--END: TASK CUSTOM CONTROL-->
                           </div>
                        </div>
                        <!--END OF CARD BODY-->
                        <!--BEGIN: CARD FOOTER-->
                        <div class="card-footer d-flex justify-content-between">
                            <div>
                                <input type="submit" class="btn btn-primary" name="best_add_tasks" value="Submit">
                            </div>
                            <div class="form-group row">
                                <label for="totalTasks" class="col-sm-4 col-form-label">Total Tasks</label>
                                <div class="col-sm-8">
                                    <input type="text" readonly class="form-control" id="bestTotalTasks" value="1" name="bestTotalTasks">
                                </div>
                            </div>
                        </div>
                        <!--END: CARD FOOTER-->
                    </form>
                  </div>
                <!--END OF CARD-->

                  <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">
                            <i class="fas fa-plus"></i>Tasks
                        </h6>
                        <button type="button"
                                onclick="addTask();"
                                class="d-sm-inline-block btn btn-sm btn-secondary shadow-sm">
                            <i class="fas fa-plus fa-sm text-white"></i>Add Task
                        </button>
                    </div>
                    <!--END OF CARD HEADER-->
                    
                    <!--CARD BODY-->
                    <form action="{{ route('task.store') }}" method="POST">
                        @csrf
                        <div class="card-body">
                           <div id="tasks_container">
                            <!--BEGIN: TASK CUSTOM CONTROL-->
                            <div class="row task_row" id="element_1">
                                <!--BEGIN: TEAM MEMBER SELECT-->
                                <div class="col-md-3">
                                    <div class="form-group">
                                       <label for="">Team Member</label>
                                       <select name="member[]" id="member_1" class="form-control member_select  @error('member') is-invalid @enderror" value="{{ old('member') }}">
                                            <option  disabled selected>Select Members</option>
                                            @foreach($users_id as $userid)
                                                    <?php
                                                        $user = App\User::where([
                                                            'id' => $userid->users_id,
                                                            'role' => 'member'
                                                        ])->get();
                                                    ?>
                                                    @if(count($user) > 0)
                                                        <option value="{{$user[0]->id}}">{{$user[0]->name}}</option>
                                                    @endif
                                            @endforeach
                                       </select>
                                       @error('member')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <!--END: TEAM MEMBER SELECT-->
                                 
                                <!--BEGIN: TASK-->
                                <div class="col-md-8">
                                    <div class="form-group">
                                       <label for="">Task</label>
                                       <input type="text" name="task[]" id="task_1"
                                               class="form-control task  @error('task') is-invalid @enderror"
                                               placeholder="Enter the task to be assigned"  value="{{ old('task') }}">
                                        @error('task')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <!--END: TASK-->
                                
                                <!--END: Final Rate PRICE-->
                                <!--BEGIN: DELETE BUTTON-->
                                <div class="col-md-1">
                                   <button onclick="deleteTask(1)"
                                       type="button"
                                        class="btn btn-danger"
                                        style="margin-top: 45%">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                </div>
                                <!--END: DELETE BUTTON-->
                            </div>
                            <!--END: TASK CUSTOM CONTROL-->
                           </div>
                        </div>
                        <!--END OF CARD BODY-->
                        <!--BEGIN: CARD FOOTER-->
                        <div class="card-footer d-flex justify-content-between">
                            <div>
                                <input type="submit" class="btn btn-primary" name="add_tasks" value="Submit">
                            </div>
                            <div class="form-group row">
                                <label for="totalTasks" class="col-sm-4 col-form-label">Total Tasks</label>
                                <div class="col-sm-8">
                                    <input type="text" readonly class="form-control" id="totalTasks" value="1" name="total_tasks">
                                </div>
                            </div>
                        </div>
                        <!--END: CARD FOOTER-->
                    </form>
                  </div>
                <!--END OF CARD-->
              </div>
          </div>
      </div>
    </div>
    <!-- /.container-fluid -->

  </div>
  <!-- End of Main Content -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->
</div>
<!-- /.container-fluid -->

@endsection

@section('page-level-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/js/all.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.select2-use').select2({
                placeholder: 'Select Team Members...'
            });
        });
    </script>

    <script>
        var id = 1;
        var total_tasks = 0;
        var best_id = 1;

        function deleteTask(delete_id){
            var elements = document.getElementsByClassName("task_row");
            if(elements.length != 1){
                total_tasks = elements.length - 1;
                $("#element_"+delete_id).remove();
                id--;
                $('#totalTasks').val(`${total_tasks}`);
            }
        }

        function deleteBestTask(delete_id){
            var elements = document.getElementsByClassName("best_task_row");
            if(elements.length != 1){
                total_tasks = elements.length - 1;
                $("#element_"+delete_id).remove();
                best_id--;
                $('#bestTotalTasks').val(`${total_tasks}`);
            }
        }

        function addTask(){
            id++;
            $('#tasks_container').append(
                `<!--BEGIN: TASK CUSTOM CONTROL-->
                            <div class="row task_row" id="element_${id}">
                                <!--BEGIN: TEAM MEMBER SELECT-->
                                <div class="col-md-3">
                                    <div class="form-group">
                                       <label for="">Team Member</label>
                                       <select name="member[]" id="member_${id}" class="form-control member_select  @error('member') is-invalid @enderror" value="{{ old('member') }}">
                                            <option  disabled selected>Select Members</option>
                                            @foreach($users_id as $userid)
                                                    <?php
                                                        $user = App\User::where([
                                                            'id' => $userid->users_id,
                                                            'role' => 'member'
                                                        ])->get();
                                                    ?>
                                                    @if(count($user) > 0)
                                                        <option value="{{$user[0]->id}}">{{$user[0]->name}}</option>
                                                    @endif
                                            @endforeach
                                       </select>
                                       @error('member')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <!--END: TEAM MEMBER SELECT-->
                                 
                                <!--BEGIN: TASK-->
                                <div class="col-md-8">
                                    <div class="form-group">
                                       <label for="">Task</label>
                                       <input type="text" name="task[]" id="task_${id}"
                                               class="form-control task  @error('task') is-invalid @enderror"
                                               placeholder="Enter the task to be assigned"  value="{{ old('task') }}">
                                        @error('task')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <!--END: TASK-->
                                
                                <!--END: Final Rate PRICE-->
                                <!--BEGIN: DELETE BUTTON-->
                                <div class="col-md-1">
                                   <button onclick="deleteTask(${id})"
                                       type="button"
                                        class="btn btn-danger"
                                        style="margin-top: 45%">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                </div>
                                <!--END: DELETE BUTTON-->
                            </div>
                            <!--END: TASK CUSTOM CONTROL-->`
            );
            $('#totalTasks').val(`${id}`); 
            
        }

        function addBestMemberTask()
        {
            best_id++;
            $("#best_tasks_container").append(
                `<!--BEGIN: TASK CUSTOM CONTROL-->
                            <div class="row best_task_row" id="element_${best_id}">
                                <!--BEGIN: TEAM MEMBER SELECT-->
                                <div class="col-md-3">
                                    <div class="form-group">
                                       <label for="">Team Member</label>
                                       <select name="member[]" id="member_${best_id}" class="form-control member_select" readonly>
                                            <option value="{{$best_member_user->id}}" selected>{{ $best_member_user->name }}</option>
                                       </select>
                                    </div>
                                </div>
                                <!--END: TEAM MEMBER SELECT-->
                                <!--BEGIN: TASK-->
                                <div class="col-md-8">
                                    <div class="form-group">
                                       <label for="">Task</label>
                                       <input type="text" name="task[]" id="task_${best_id}"
                                               class="form-control task  @error('task') is-invalid @enderror"
                                               placeholder="Enter the task to be assigned"  value="{{ old('task') }}">
                                        @error('task')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <!--END: TASK-->
                                
                                <!--BEGIN: DELETE BUTTON-->
                                <div class="col-md-1">
                                   <button onclick="deleteBestTask(${best_id})"
                                       type="button"
                                        class="btn btn-danger"
                                        style="margin-top: 45%">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                </div>
                                <!--END: DELETE BUTTON-->
                            </div>
                            <!--END: TASK CUSTOM CONTROL-->`
            );
            $('#bestTotalTasks').val(`${best_id}`); 
        }
        
    </script>

   
@endsection

@section('page-level-styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
@endsection