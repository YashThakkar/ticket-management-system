
@extends('layouts.components.layout')
@section('title','Ticket management | Ease to manage')

<?php
$notification_count = $notifications->count(); 

?>
@section('notifications')
    @if($notification_count > 0)
        <span class="badge badge-pill badge-primary"> {{ $notifications->count() }}  </span>
    @endif
@endsection

@section('notification-box-contents')
    @if($notification_count > 0)
        @foreach($notifications as $notification)
            <p>{{ $notification->data["user"] }} has submitted task</p>
            <div class="mx-3 mb-2" style="width:80%;background-color:#F5F5F5;height:1px;opacity: 0.3;"></div>
        @endforeach
    @else
        <p class="m-0 py-1" style="font-weight: 200;">No Notifications yet! <span class="fas fa-exclamation-circle"></span> </p>
    @endif
@endsection

@section('main-content')

<div class="container mt-4 mx-2">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between">
    <h1 class="h3 mb-4 text-gray-800">Team <span class="color-blue">{{$team[0]->team_name}}</span></h1>
    <a href="" class="d-none d-sm-inline-nlock btn btn-sm btn-primary shadow-sm">
        <i class="fas fa-list-ul fa-sm text-white"></i>
    </a>                  
    </div>
    <!-- <div class="card text-white bg-dark mb-3" style="max-width: 100%;"> -->
    <!-- <div class="card-body"> -->
        <h5 class="card-title ml-2">{{$team[0]->team_leader_name}} - Leader</h5>

        <!-- <div class="row">
            <div class="col-md-4 leader-cards">
                <div class="name-box" >
                    <p>Lorem, ipsum dolor.</p>
                </div>
            </div>
        </div> -->

        @foreach($users_in_team as $user)
            <!-- <p class="card-text"> -->
               
                <?php
                    $user_info = App\User::where([
                        'id' => $user->users_id,
                        'role' => 'member'
                    ])->get();
                // dd($user_info[0]->name);
                $color = rand(1,3);
                ?>
                
                @if(count($user_info) > 0)
                    <div class="row ml-2">
                        <div class="col-md-4 leader-cards my-3 shadow p-0" style="border-radius:0 8px 8px 0!important;">
                            <div class="name-box border_color_{{ $color }}">
                                <div class="row p-3">
                                    <div class="col-md-6 ">
                                        <p class="m-0 member-name">{{ $user_info[0]->name }}</p>
                                    </div>
                                    <div class="col-md-6">
                                    <p class="member-role">{{ $user_info[0]->role }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            <!-- </p> -->
        @endforeach
    <!-- </div> -->
    <!-- </div> -->
</div>
@endsection

<!-- <button type="button" class="btn btn-primary">
  Notifications <span class="badge badge-light">4</span>
</button> -->
