
@extends('layouts.components.layout')
@section('title','Ticket management | Ease to manage')

@section('main-content')
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between">
  <h1 class="h3 mb-4 text-gray-800">Add Category</h1>
  <a href="" class="d-none d-sm-inline-nlock btn btn-sm btn-primary shadow-sm">
      <i class="fas fa-list-ul fa-sm text-white"></i>
  </a>                  
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header">
                    <h6 class="m-0 font-weight-bold text-primary">
                        <i class="fa fa-plus"></i>Create Team
                    </h6>
                </div>
              <!--END OF CARD HEADER-->
              
              <!--CARD BODY-->
              <div class="card-body">
                  <form action="{{ route('team.store') }}" method="POST">
                     @csrf
                      <div class="row" id="create_container">
                          <div class="col-md-6">
                              <div class="form-group">
                                <label for="team_name">Team Name</label>
                                <input type="text"
                                    value="{{ old('team_name') }}"
                                    class="form-control @error('team_name') is-invalid @enderror"
                                    name="team_name" id="team_name">
                                @error('team_name')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="team_leader">Team Leader</label>
                                <select name="team_leader[]"  class="form-control select2 team_leader select_leader">
                                    
                                </select>
                                @error('team_leader')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                                <label for="team_members">Team Members</label>
                                <select name="team_members[]" id="team_members"style="height: 124px;" class="form-control select2-use select-member" multiple="multiple">
                                    @foreach($users as $user)
                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                    @endforeach
                                </select>
                                @error('team_members')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                          </div>
                  </div>
                  <div class="form-group">
                    <input type="submit" class="btn btn-primary " name="create-team" value="Create" id="create-team">
                  </div>
                  
                  </form>
              </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- /.container-fluid -->

@endsection

@section('page-level-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.select2-use').select2({
                placeholder: 'Select Team Members...'
            });
        });
    </script>

    <script>
        $("#create_container").on('change', '.team_members', function(){
            alert("change");
        });
    </script>

    <script>
        // on select event
        $('.select-member').on('select2:select', function (e) {
            var data = e.params.data;
            // console.log(data.text);

            var newdata = {
                id: data.id,
                text: data.text
            };

            var newOption = new Option(newdata.text, newdata.id, false, false);
            $('.select_leader').append(newOption).trigger('change');

        });

        // on unselect event
        $('.select-member').on('select2:unselect', function (e) {
            var data = e.params.data;
            
            $(".select_leader").empty();
            var selected_info = $('.select-member').select2('data');
            var i;
            for(i=0;i<selected_info.length;i++){
                var newOption = new Option(selected_info[i].text, selected_info[i].id, false, false);
                $('.select_leader').append(newOption).trigger('change');
            }
        });
    </script>
@endsection

@section('page-level-styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css">
@endsection