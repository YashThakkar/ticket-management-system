
@extends('layouts.components.layout')
@section('title','Ticket management | Ease to manage')

@section('main-content')

<div class="container mt-4">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between">
    <h1 class="h3 mb-4 text-gray-800">All Teams</h1>
    <a href="" class="d-none d-sm-inline-nlock btn btn-sm btn-primary shadow-sm">
        <i class="fas fa-list-ul fa-sm text-white"></i>
    </a>                  
    </div>

    @foreach($teams as $team)
        <div class="card text-white bg-dark mb-3" style="max-width: 100%;">
        <div class="card-header">{{$team->team_name}}</div>
        <div class="card-body">
            <h5 class="card-title">{{$team->team_leader_name}} - Leader</h5>

<?php
    $users_in_team = App\User_teams::where('teams_id', $team->id)->get();
?>
            @foreach($users_in_team as $user)
                <!-- <p class="card-text"> -->
                    <?php
                        $user_info = App\User::where([
                            'id' => $user->users_id,
                            'role' => 'member'
                        ])->get();
                    // dd($user_info[0]->name);
                    ?>
                    @if(count($user_info) > 0)
                        <span class="btn badge-primary">{{ $user_info[0]->name }} / {{ $user_info[0]->role }}</span>
                    @endif
                <!-- </p> -->
            @endforeach
        </div>
        </div>
    @endforeach
</div>
@endsection

<!-- <button type="button" class="btn btn-primary">
  Notifications <span class="badge badge-light">4</span>
</button> -->
