<div class="col-md-2" id="sidebar">
    <div class="logo-area">
        <p class="m-0">Ticket<span class="main-blue"> Management</span></p> 
        <!-- <hr> -->
    </div>
    <div class="div mt-5">
        <ul>
            <!-- Showing all teams -->
           @if(auth()->user()->isAdmin())
                <li><a href="{{ route('admin.index') }}"><span class="fas fa-home"></span>&nbsp; Home</a></li>
                <li><a href="{{ route('team.index') }}"><span class="fas fa-users"></span>&nbsp; All Teams</a></li>
                <li><a href="{{ route('team.create') }}"><span class="fas fa-plus"></span>&nbsp; Create Team</a></li>
            @elseif(auth()->user()->isLeader())
            <li><a href="{{ route('leader.index') }}">Home</a></li> 
                <li><a href="{{ route('task.create') }}"><span class="fas fa-plus"></span>&nbsp; Assign Task</a></li>
                <li>
                    <a href="{{ route('task.index') }}"><span class="fas fa-list"></span>&nbsp; View Tasks</a>
                    
                    @yield('notifications')
                </li>
           @else
                <li><a href="{{ route('member.index') }}"><span class="fas fa-home"></span>&nbsp; Home</a></li>
                <!-- <li><a href="#"><span class=""></span> View Task</a></li> -->
            @endif
            <li><a href="#">Other</a></li>
        </ul>

        @if(auth()->user()->isLeader())
            @if(URL::current() == 'http://localhost:8000/leader')
                <div class="notification-box bg-primary p-3" style="border-radius:10px;margin-top:150px;">
                    <p class="color-light pb-1 m-0" style="font-weight: 600;">Notifications:</p>
                    <div class="mb-2" style="margin-left:-20px;width:120%;background-color:#000C15;height:1px;opacity: 0.1;"></div>
                    @yield('notification-box-contents')
                </div>
            @endif
        @endif

    </div>
</div>

@section('page-level-styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
@endsection

@section('page-level-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/js/all.min.js"></script>
@endsection