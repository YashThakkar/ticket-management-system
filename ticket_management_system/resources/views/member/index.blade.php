
@extends('layouts.components.layout')
@section('title','Ticket management | Ease to manage')

@section('main-content')

<div class="container mt-4">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between">
    <h1 class="h3 mb-4 text-gray-800">All Tasks <span class=""></span></h1>
    <a href="" class="d-none d-sm-inline-nlock btn btn-sm btn-primary shadow-sm">
        <i class="fas fa-list-ul fa-sm text-white"></i>
    </a>                  
    </div>

        

                <div class="card shadow mb-3" style="max-width: 100%; border-radius: 12px!important">
                    <div class="card-header" style="border-top-left-radius: 12px!important;border-top-right-radius: 12px!important;">
                        Tasks assigned by - {{ $leader_name }} (Leader)
                    </div>
                    <div class="card-body">
                        <h5 class="card-title" style="color: #3857F5;">Tasks</h5>

                        @if ( sizeOf($tasks) <= 0 )
                            <p class="text-muted">No tasks assigned yet :)</p>
                        @endif

                        @foreach($tasks as $task)
                            <div class="row mt-2">
                                <?php
                                    $color = rand(1,3);
                                ?>
                                <div class="col-md-12 pr-4">
                                    <div class="task-bar  border_color_{{ $color }}">
                                        <div class="row">
                                            <div class="col-md-10 py-3">
                                                <span class="px-3 m-0 py-2">{{ $task->task }}</span> &nbsp;
                                                @if($task->task_status === 'pending')
                                                    <span class="fas fa-exclamation-circle color-red"></span>
                                                @endif
                                            </div>
                                            @if($task->task_status === 'pending')
                                                <div class="col-md-2 status-outer outline-green p-0 outline-green-hover">
                                                    <form action="{{ route('task.update_to_approval', $task) }}" method="POST">
                                                        @csrf
                                                        @method('PUT')
                                                        <button type="submit" class=" btn p-3 color-green">Mark as Submitted &nbsp;<span class="fas fa-check"></span> </button>
                                                    </form>
                                                </div>
                                            @elseif($task->task_status === 'approval_pending')
                                                <div class="col-md-2 status-outer bg-warning p-0">
                                                    <p class="p-3 m-0">Approval Pending &nbsp;<span class="fas fa-check-circle"></span></p>
                                                </div>
                                            @elseif($task->task_status === 'completed')
                                                <div class="col-md-2 status-outer bg-success p-0">
                                                    <p class="p-3 m-0" style="color: #fff;">Task Completed &nbsp;<span class="fas fa-check"></span></p>
                                                </div>
                                            @endif
                                        </div>
                                    </div>   
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            
</div>
@endsection


@section('page-level-styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
@endsection

