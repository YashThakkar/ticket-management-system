
@extends('layouts.components.layout')
@section('title','Ticket management | Ease to manage')

@section('main-content')

    <div class="container-fluid mt-4 admin-index p-0" style="background-color: #F8F9FC!important;">
        <div class="row">
            <div class="col-md-12">
                <h4>Statistics</h4>

                <div class="row mx-0 my-4">
                    <div class="col-xl-3 total_teams_box shadow p-3 ml-4 border-4_color_blue">
                        <p class="box-title m-0 color-blue">Teams</p>
                        <div class="row mr-2">
                            <div class="col-md-2">
                             <p class="number">{{ $teams_count }}</p>
                            </div>
                            <div class="col-md-9 p-2 mt-1" style="padding-left: 0px!important;">
                                <div class="progress m-0" style="border-radius: 10px!important; height: 11px!important;width: 190px;">
                                    <div class="progress-bar" role="progressbar" style="width: {{ $teams_count }}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <div class="col-md-1 p-0">
                                <span class="fas fa-users-cog" style="color: #DDDFEB; font-size: 28px"></span>
                            </div>
                        </div>
                    </div>
                
                    <div class="col-xl-3 total_teams_box shadow p-3 ml-3 border-4_color_red">
                        <p class="box-title m-0 color-red">Members</p>
                        <div class="row mr-2">
                            <div class="col-md-2">
                             <p class="number">{{$member_count}}</p>
                            </div>
                            <div class="col-md-9 p-2 mt-1" style="padding-left: 0px!important;">
                                <div class="progress m-0" style="border-radius: 10px!important; height: 11px!important;width: 190px;">
                                    <div class="progress-bar" role="progressbar" style="width: {{$member_count}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <div class="col-md-1 p-0">
                                <span class="fas fa-users" style="color: #DDDFEB; font-size: 28px"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 total_teams_box shadow p-3 ml-3 border-4_color_yellow">
                        <p class="box-title m-0 color-yellow">Tasks Completed</p>
                        <div class="row mr-2">
                            <div class="col-md-2">
                             <p class="number">{{$task_completed_count}}</p>
                            </div>
                            <div class="col-md-9 p-2 mt-1" style="padding-left: 0px!important;">
                                <div class="progress m-0" style="border-radius: 10px!important; height: 11px!important;width: 190px;">
                                    <div class="progress-bar" role="progressbar" style="width: {{$task_completed_count}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <div class="col-md-1 p-0">
                                <span class="fas fa-clipboard-check" style="color: #DDDFEB; font-size: 28px"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 total_teams_box shadow p-3 ml-3 border-4_color_green">
                        <p class="box-title m-0 color-green">Tasks Pending</p>
                        <div class="row mr-2">
                            <div class="col-md-2">
                             <p class="number">{{$task_pending_count}}</p>
                            </div>
                            <div class="col-md-9 p-2 mt-1" style="padding-left: 0px!important;">
                                <div class="progress m-0" style="border-radius: 10px!important; height: 11px!important;width: 190px;">
                                    <div class="progress-bar" role="progressbar" style="width: {{$task_pending_count}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <div class="col-md-1 p-0">
                                <span class="fas fa-user-clock" style="color: #DDDFEB; font-size: 28px"></span>
                            </div>
                        </div>
                    </div>  
                </div>
                <div class="row mt-4">
                    <div class="col-md-7 ml-4">
                        <div class="card">
                            <div class="card-body">
                                <canvas id="admin-chart"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 ml-5 px-2">
                        <div class="card bg-primary text-white text-center p-3" style="border-radius: 15px;">
                            <blockquote class="blockquote mb-0">
                                <h4>Tip & Tricks</h4>
                                <small>Good member is always selected with its capability & efficiency to do work but not just completing.</small>
                                <footer class="blockquote-footer text-white">
                            </footer>
                            </blockquote>
                        </div>
                        <div class="card bg-light mb-3 my-4" >
                            <div class="card-header">Admin <span class="text-danger"><i class="fas fa-bullhorn"></i></span></div>
                            <div class="card-body">
                              <h5 class="card-title">Responsibilities</h5>
                              <p class="card-text">Reverting asap after completion of every assigned task to your head is your responsibility.<br><br> <span class="text-secondary">Thank You!</span> </p>
                            </div>
                          </div>
                    </div>
                    <div class="col-md-4 ml-5 px-2">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" value="{{ $teams_count }}" id="team-count-for-js">
    @foreach ($teams as $team)
        <input type="hidden" value="{{ $team->team_name }}" id='team-for-js_{{$team->id}}' class="team-name-js">
    @endforeach
    
@endsection

@section('page-level-scripts')
    <script>
        var myChart;
        let teamCount = parseInt($("#team-count-for-js").val());

        var teamNames = [];
        $('.team-name-js').each(function(i, obj) {
            teamNames.push(obj.value);
        });
        // console.log(teamNames);
        
        var colors = ['#007bff','#28a745','#333333','#c3e6cb','#dc3545','#6c757d'];

        /* large line chart */
        var chLine = document.getElementById("admin-chart");
        var chartData = {
            labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            
            datasets: []
            // datasets: 
            // [
            //     {
            //         data: [589, 445, 483, 503, 689, 692, 634],
            //         backgroundColor: 'transparent',
            //         borderColor: colors[Math.floor(Math.random() * 5)],
            //         borderWidth: 4,
            //         pointBackgroundColor: colors[Math.floor(Math.random() * 5)]
            //     },
            //     {
            //         data: [639, 465, 493, 478, 589, 632, 674],
            //         backgroundColor: colors[3],
            //         borderColor: colors[Math.floor(Math.random() * 5)],
            //         borderWidth: 4,
            //         pointBackgroundColor: colors[Math.floor(Math.random() * 5)]
            //     }
            // ]
        };

        

        if (chLine) {
            myChart = new Chart(chLine, {
            type: 'line',
            data: chartData,
            options: {
                scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero: false
                    }
                }]
                },
                legend: {
                display: false
                },
                responsive: true
            }
            });

            updateChart();
        }

        function createNewDataset() {
            var newDataset = {
                data: [],
                label: "",
                backgroundColor: colors[3],
                borderColor: colors[Math.floor(Math.random() * 5)],
                borderWidth: 4,
                pointBackgroundColor: colors[Math.floor(Math.random() * 5)]
            }
            return newDataset;
        }

        function getRandomArray() {
            var data = [
                        Math.floor(Math.random() * 5), 
                        Math.floor(Math.random() * 10),
                        Math.floor(Math.random() * 150),
                        Math.floor(Math.random() * 200),
                        Math.floor(Math.random() * 100),
                        Math.floor(Math.random() * 300),
                        Math.floor(Math.random() * 400),
                        Math.floor(Math.random() * 350),
                        Math.floor(Math.random() * 200),
                        Math.floor(Math.random() * 310),
                        Math.floor(Math.random() * 380),
                        Math.floor(Math.random() * 480)
                    ];
            return data;
        }

        function updateChart() {
            for(let i=0; i < teamCount; i++ ) {
                chartData.datasets.push(createNewDataset());
                myChart.update();
                chartData.datasets[i].data = getRandomArray();//[100, 465, 493, 478, 589, 632, 674];
                chartData.datasets[i].label = teamNames[i];
                
                myChart.update();
            }
            // console.log(chartData.datasets);
        }
        
    </script>
@endsection

